package com.richbighouse.mylistapp;

public class Food {

    private int mImageDrawable;
    private String name;
    private String country;

    public Food(String name, String country) {
        this.name = name;
        this.country = country;
    }

    public int getmImageDrawable() {
        return mImageDrawable;
    }

    public void setmImageDrawable(int mImageDrawable) {
        this.mImageDrawable = mImageDrawable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
