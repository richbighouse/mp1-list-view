package com.richbighouse.mylistapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

class CustomAdapter extends ArrayAdapter {

    public CustomAdapter(@NonNull Context context, List<Food> foods) { //resource = array
        super(context, R.layout.class_row, foods);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater customInflator = LayoutInflater.from(getContext()); //Inflater = prepare
        View customRow = customInflator.inflate(R.layout.class_row, parent, false);

        Food foodItem =  (Food) getItem(position);

        TextView textName = customRow.findViewById(R.id.classRowNameId);
        TextView textCountry = customRow.findViewById(R.id.classRowCountryId);
        ImageView imageView = customRow.findViewById(R.id.classRowImageId);

        textName.setText(foodItem.getName());
        textCountry.setText(foodItem.getCountry());

        imageView.setImageResource(R.drawable.bananas);

        return  customRow;
    }
}
