package com.richbighouse.mylistapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class InputActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);

        final List<Food> foods = new ArrayList<>();
        final ListView inputListView = findViewById(R.id.input_listView);

        final ListAdapter customAdapter = new CustomAdapter(this, foods);
        inputListView.setAdapter(customAdapter);

        Button add = findViewById(R.id.input_button);
        final EditText editTextName = findViewById(R.id.input_name);
        final EditText editTextCountry = findViewById(R.id.input_country);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = editTextName.getText().toString();
                String country = editTextCountry.getText().toString();
                Food food = new Food(name, country);
                foods.add(food);
                inputListView.invalidateViews();
            }
        });

        inputListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String text = "I have removed " + foods.get(i).getName();
                foods.remove(i);
                Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG);
                toast.show();
                inputListView.invalidateViews();
            }
        });
    }
}
