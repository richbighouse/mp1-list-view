package com.richbighouse.mylistapp;

import android.app.Activity;
import android.database.DataSetObserver;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //String[] names = {"Jon", "Kayla", "Mandeep", "Victoria"};
        List<String> names = new ArrayList<>();
        names.add("Jon");
        names.add("Becky");
        names.add("Rajeesh");
        names.add("Julia");
        names.add("David");

        final ListAdapter myAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, names);
        ListView myListView = findViewById(R.id.myListView);
        myListView.setAdapter(myAdapter);

        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String item = (String) myAdapter.getItem(i);
                Toast toast = Toast.makeText(getApplicationContext(), "Hi " + item, Toast.LENGTH_LONG);
                toast.show();
            }
        });
    }
}
